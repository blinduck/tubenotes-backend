from api.models import Playlist
from rest_framework import serializers
from api.serializers import NoteListSerializer

class PlaylistListSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True)
    owner = serializers.PrimaryKeyRelatedField(read_only=True)
    notes = serializers.SlugRelatedField(many=True, read_only=True, slug_field='uid')

    class Meta:
        model = Playlist
        exclude = ['id',]


class PlaylistDetailSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True)
    owner = serializers.PrimaryKeyRelatedField(read_only=True)
    notes = NoteListSerializer(many=True)

    class Meta:
        model = Playlist
        exclude = ['id',]


