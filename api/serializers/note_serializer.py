from api.models import Note
from rest_framework import serializers

class NoteListSerializer(serializers.ModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(read_only=True)
    title = serializers.CharField(required=True)


    class Meta:
        model = Note
        fields = ["uid", "title", "video_code", "owner",
                  "step_count", "created_at", "is_private"]


class NoteDetailSerializer(serializers.ModelSerializer):
    # owner = serializers.PrimaryKeyRelatedField(read_only=True)
    owner_id = serializers.IntegerField(read_only=True, required=False)


    class Meta:
        model = Note
        exclude = ["id", "owner"]



