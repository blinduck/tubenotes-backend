from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('signup', views.signup),
    path('login', views.login),
    path("public/notes", views.PublicNoteList.as_view()),
    path("notes", views.NoteList.as_view()),
    path("notes/<uid>", views.RetrieveNoteDetail.as_view()),
    path("notes/edit/<uid>", views.UpdateDestroyNoteDetail.as_view()),
    path("collections", views.PlaylistList.as_view()),
    path("collections/<uid>", views.PlaylistDetail.as_view()),
    path("collections/<collection_uid>/add", views.add_to_playlist),
    path('sentry-debug', views.trigger_error),

]