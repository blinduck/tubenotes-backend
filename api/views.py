from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.http import HttpResponse
from django.shortcuts import render
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

# TODO  implement caching of public note list


from api.models import Note, Playlist
from api.serializers import UserSerializer, PlaylistListSerializer, PlaylistDetailSerializer
from api.serializers.note_serializer import NoteDetailSerializer, NoteListSerializer
from utils import youtube_utils
from django.db.models import Q

# Create your views here.
from rest_framework import generics, status, mixins

def trigger_error(request):
    division_by_zero = 1 / 0


def index(request):
    return HttpResponse("Hello world")


class PublicNoteList(generics.ListAPIView):
    serializer_class = NoteListSerializer
    authentication_classes = []
    permission_classes = []

    def get_queryset(self):
        return Note.objects.filter(is_private=False).order_by("-created_at")


class NoteList(generics.ListCreateAPIView):
    serializer_class = NoteListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]


    def get_queryset(self):
        return Note.objects.filter(owner=self.request.user).order_by("-created_at")

    def perform_create(self, serializer):
        # TODO Get original title so that we can implement search later.
        # Probably can be done as a asynchronous process as title is being passed in
        # from FE now
        # video_code = serializer.initial_data["video_code"]
        # title = youtube_utils.extract_title(video_code)
        serializer.save(owner=self.request.user)

class RetrieveNoteDetail(generics.RetrieveAPIView):
    serializer_class = NoteDetailSerializer
    authentication_classes = []
    permission_classes = []
    lookup_field = "uid"

    def get_queryset(self):
        return Note.objects.all()

    # def get(self, request, *args, **kwargs):
    #     return self.retrieve(request, *args, **kwargs)

class UpdateDestroyNoteDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = NoteDetailSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    lookup_field = "uid"

    def get_queryset(self):
        return Note.objects.filter(owner=self.request.user)

    # def get(self, request, *args, **kwargs):
    #     return self.retrieve(request, *args, **kwargs)

class PlaylistList(generics.ListCreateAPIView):
    serializer_class = PlaylistListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Playlist.objects.filter(owner=self.request.user).order_by('-created_at')

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class PlaylistDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PlaylistDetailSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    lookup_field = "uid"

    def get_queryset(self):
        return Playlist.objects.filter(owner=self.request.user)




@api_view(['POST'])
@permission_classes([AllowAny])
def add_to_playlist(request, *args, **kwargs):
    note_uid = request.data['note_uid']
    playlist_uid = kwargs['collection_uid']

    playlist = Playlist.objects.get(uid=playlist_uid)
    note = Note.objects.get(uid=note_uid)

    if (playlist.owner != request.user):
        return Response( { 'error': 'Playlist not owned' }, status.HTTP_401_UNAUTHORIZED)

    playlist.notes.add(note)
    return Response({'msg': "Successfully added to playlist"})



@api_view(['POST'])
@permission_classes([AllowAny])
def signup(request):
    username = request.data['email']
    password = request.data['password']

    try:
        user = User.objects.create_user(username, username, password)
        ser = UserSerializer(instance=user)
        return Response(ser.data, status.HTTP_200_OK, content_type="application/json")
    except IntegrityError  as e:
        return Response(
            data={"msg": "User with that username already exists"},
            status=status.HTTP_400_BAD_REQUEST, content_type="application/json")


@api_view(['POST'])
@permission_classes([AllowAny])
def login(request):
    username = request.data['email']
    password = request.data['password']

    if username and password:
        user = authenticate(username=username, password=password)
        if user:
            ser = UserSerializer(user)
            return Response(ser.data, status.HTTP_200_OK)
        else:
            return Response({"msg": "Wrong username + password combination"}, status.HTTP_400_BAD_REQUEST)

    return Response(data={"msg": "Username/Password not provided"},  status=status.HTTP_400_BAD_REQUEST)
