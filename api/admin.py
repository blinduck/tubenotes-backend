from django.contrib import admin

# Register your models here.

from .models import Note, Playlist


class NoteAdmin(admin.ModelAdmin):
    list_display = ("id", "uid", "title", "is_private", "owner")


class PlaylistAdmin(admin.ModelAdmin):
    list_display = ('id', 'uid', 'name', 'owner')
    pass

admin.site.register(Note, NoteAdmin)
admin.site.register(Playlist, PlaylistAdmin)
