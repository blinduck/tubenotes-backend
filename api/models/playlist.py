from django.contrib.auth.models import User
from django.db import models
import uuid

# Collection for now, unordered
class Playlist(models.Model):
    uid = models.UUIDField(default=uuid.uuid4, editable=False, db_index=True)
    name = models.CharField(max_length=100)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    notes = models.ManyToManyField('Note')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

