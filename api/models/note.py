from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
import uuid


class Note(models.Model):
    uid = models.UUIDField(default=uuid.uuid4, editable=False, db_index=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    video_code = models.CharField(max_length=100)
    title = models.TextField()
    original_title = models.TextField(default="") # meant to be used in future for searching?
    steps = JSONField(default=list, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    step_count = models.IntegerField(default=0)
    is_private = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.step_count = len(self.steps)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)