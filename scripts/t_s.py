from django.contrib.auth.models import User

from api.serializers import UserSerializer

def run():
    u =  User.objects.get(username="test")
    print(u)

    ser = UserSerializer(instance=u)
    print(ser.data)