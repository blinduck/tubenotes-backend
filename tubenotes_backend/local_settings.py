
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        # 'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tubenotes-db',
        'USER': 'tubenotes',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'POST': '5432'
    }
}


DEBUG = True
ALLOWED_HOSTS = ["*",]
