import requests
import json
from urllib import parse

# TODO Does not seem to be working any longer
def extract_title(video_id):
    resp = requests.get("http://youtube.com/get_video_info?video_id=" + video_id)
    if resp.status_code != 200:
        return ""
    try:
        s = parse.parse_qs(resp.text)
        pr = json.loads(s["player_response"][0])
        return pr["videoDetails"]["title"]

    except Exception as e:
        return ""

